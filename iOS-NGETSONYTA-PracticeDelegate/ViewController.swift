//
//  ViewController.swift
//  iOS-NGETSONYTA-PracticeDelegate
//
//  Created by Nyta on 11/3/20.
//

import UIKit

protocol MessageDelegate{
    func didSend(msg: String)
}

class ViewController: UIViewController {

    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var doRoundedButton: UIButton!
    
    var delegate: MessageDelegate? //Step 2
    var colorBackground: UIColor = .white
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doRoundedButton.layer.cornerRadius = 20
        
    }

    @IBAction func buttonPressed(_ sender: Any) {
        let message = txtMessage.text!
        let secondView = storyboard?.instantiateViewController(withIdentifier: "SecondView") as! SecondViewController
        
        secondView.delegate = self
        self.delegate = secondView
        secondView.modalPresentationStyle = .fullScreen
        self.delegate?.didSend(msg: message)
        present(secondView, animated: true, completion: nil)
    }
    
}

extension ViewController: BackgroundDelegate{
  
    func didUpdateBackground(image: UIImage, name: String, color: UIColor) {
        firstImage.image = image
        txtMessage.text = name
        self.view.backgroundColor = color
    }
}
