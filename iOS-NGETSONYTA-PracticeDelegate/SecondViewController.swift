//
//  SecondViewController.swift
//  iOS-NGETSONYTA-PracticeDelegate
//
//  Created by Nyta on 11/3/20.
//

import SwiftUI


protocol BackgroundDelegate{
    func didUpdateBackground(image: UIImage, name: String, color: UIColor)
}

class SecondViewController: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
   
    var delegate: BackgroundDelegate?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btnTrash(_ sender: UIButton) {
        self.view.backgroundColor = .orange
        delegate?.didUpdateBackground(image: UIImage(named: "GreenCake")!,name: "Orange Cupcake",color: .orange)
        dismiss(animated: true, completion: nil)
       
    }
    @IBAction func btnPeople(_ sender: UIButton) {
        
        self.view.backgroundColor = .systemPink
        delegate?.didUpdateBackground(image: UIImage(named: "PinkCake")!,name: "Pink Cupcake",color: .systemPink)
        dismiss(animated: true, completion: nil)
        
    }
}

extension SecondViewController: MessageDelegate{
    func didSend(msg: String) {
    }
}

